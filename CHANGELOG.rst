Changelog
=========

attotime 0.3.1-dev.0
====================

*Release date: YYYY-MM-DD*

attotime 0.3.0
==============

*Release date: 2021-05-27*

Changed
-------
* Explicitly type check arguments against appropriate numeric types in :code:`attodatetime.attotime.__init__`, :code:`attodatetime.attodatetime.__init__`, and :code:`attodatetime.attotimedelta.__init__`, this prevents accidental precision loss when constructing :code:`attotime` and :code:`attodatetime` objects

attotime 0.2.3
==============

*Release date: 2021-03-18*

Added
-----
* Development requirements handled by :code:`extras_require` (install with :code:`pip install -e .[dev]`)
* Pre-commit hooks, managed with `pre-commit <https://pre-commit.com/>`_ (install with :code:`pre-commit install`)
* Add :code:`readthedocs.yaml` to make configuration explicit

Changed
-------
* Code formatted with `Black <https://black.readthedocs.io/en/stable/index.html>`_
* Imports sorted with `isort <https://pycqa.github.io/isort/>`_
* Following `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_ for this and future CHANGELOG entries
* :code:`LocalTimezone.STDOFFSET`, :code:`LocalTimezone.DSTOFFSET`, and :code:`LocalTimezone.DSTDIFF` are now set in the constructor for easier testability
* :code:`attodatetime.strptime` will no longer hang in an infinite loop when an invalid format field is given in the format string
* Tests no longer fail with local system is in DST

Removed
-------
* Unused :code:`attotime.util.tuple_add`

attotime 0.2.2
==============

*Release date: 2021-02-13*

Changes
-------

* Cast to datetime when building timezone delta in :code:`attodatetime.fromtimestamp`, :code:`attodatetime.astimezone` to prevent :code:`TypeError` being thown by standard library type checks, note that this explicitly means timezones will lose attosecond precision in their offsets, which was the case anyway as Python `timezone objects <https://docs.python.org/3/library/datetime.html#timezone-objects>`_ are used internally which lack such precision

attotime 0.2.1
==============

*Release date: 2021-02-08*

Changes
-------
* Explicitly cast to int for native time and native timedelta in :code:`attotime` and :code:`attotimedelta` constructors
* Add version to :code:`version.py`
* Cleaner reading of `README.rst` into the :code:`long_description` field of `setup.py`
* Define :code:`long_description_content_type` as :code:`text/x-rst`
* Simplify Sphinx configuration
* Bump copyright date to 2021

Deprecation
-----------
* Deprecate running tests with :code:`python setup.py tests` as the test suite support in Setuptools is `deprecated <https://github.com/pypa/setuptools/issues/1684>`_

attotime 0.2.0
==============

*Release date: 2019-01-08*

Changes
-------
* Drop support for distutils
* Add support for running tests via setuptools (:code:`python setup.py test`)
* `Read the Docs <https://attotime.readthedocs.io>`_ documentation

attotime 0.1.3
==============

*Release date: 2019-01-04*

Changes
-------
* Rebuild with clean build directory

attotime 0.1.2
==============

**WITHDRAWN FROM PYPI**

*Release date: 2019-01-04*

Changes
-------
* Correctly include packages
* Correct MANIFEST.in

attotime 0.1.1
==============

**WITHDRAWN FROM PYPI**

*Release date: 2019-01-04*

Changes
-------
* Add missing MANIFEST.in

attotime 0.1.0
==============

**WITHDRAWN FROM PYPI**

*Release date: 2019-01-04*

Changes
-------
* No longer use relative imports
* Move :code:`attodatetime`, :code:`attotimedelta`, and :code:`attotime` implementation to :code:`attotime.objects` to avoid ambiguous imports, their availability through :code:`attotime.attodatetime`, :code:`attotime.attotimedelta`, and :code:`attotime.attotime` remains unchanged
* :code:`repr(attotime1)` Returns a string in the form :code:`attotime.objects.attotime(h, m, s, us, ns, [tz])`.
* :code:`repr(attodatetime1)` Returns a string in the form :code:`attotime.objects.attodatetime(Y, M, D, h, m, s, us, ns, [tz])`.
* :code:`repr(attotimedelta1)` Returns a string in the form :code:`attotime.objects.attotimedelta(D[, S[, U]])`, where :code:`D` is negative for :code:`td1 < 0`.
* Require `mock <https://github.com/testing-cabal/mock>`_ for running unit tests
* Implement :code:`attodatetime.fromtimestamp`
* Implement :code:`attodatetime.utcfromtimestamp`
* Implement :code:`attodatetime.today`
* Implement :code:`attodatetime.now`
* Implement :code:`attodatetime.utcnow`
* Implement :code:`attodatetime.astimezone`
* Implement :code:`attodatetime.strptime`
* Implement :code:`attodatetime.strftime`
* Use xrange with Python 2 via :code:`compat.range`
* Remove :code:`util.reduce`
* Remove :code:`util.decimal_split`
* Use :code:`unittest.mock` with Python 3

attotime 0.0.1
==============

*Release date: 2018-10-25*

Changes
-------
* Initial release
